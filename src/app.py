from typing import Optional
from fastapi import FastAPI, HTTPException, status
from pydantic import BaseModel
from pydantic.fields import Field
from db.db import session
from db.schemas import User, Bark
from db.db import Base, engine


app = FastAPI()

Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)


class UserRequest(BaseModel):
    user_name: str = Field(alias="userName")
    password: str 

class BarkRequest(BaseModel):
    user_id: str = Field(alias="userId")
    bark: str



@app.post("/register", status_code=status.HTTP_201_CREATED)
def register(user_request: UserRequest):
    user = User(username=user_request.user_name, password= user_request.password)
    session.add(user)
    session.commit()
    session.refresh(user)
    return user

@app.post("/login", status_code=status.HTTP_200_OK)
def login(user_request: UserRequest):
    user = session.query(User).where(User.username == user_request.user_name).first()

    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Wrong username or password")

    if user.password != user_request.password:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Wrong username or password")

    return {
        "userId": user.id
    }
    


@app.post("/bark")
def post_bark(bark_request: BarkRequest):
    bark = Bark(content=bark_request.bark, user_id=bark_request.user_id)
    session.add(bark)
    session.commit()
    session.refresh(bark)
    return bark

@app.get("/barks")
def get_barks(userId: Optional[int] = None):
    user = session.query(User).where(User.id == userId).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return user.barks
