from typing import List
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import Mapped, mapped_column, relationship
from db.db import Base

class User(Base):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    barks: Mapped[List["Bark"]] = relationship(back_populates="user")
    username = Column(String, unique=True, index=True)
    password = Column(String)

class Bark(Base):
    __tablename__ = "barks"
    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    content = Column(String)
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    user: Mapped["User"] = relationship(back_populates="barks")
